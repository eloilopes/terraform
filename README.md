# Terraform


*main.tf*

**Script adapted for Terraform 0.12**

**Errors after upgrade**


> Error: Attribute redefined
> 
>   on main.tf line 60, in resource "oci_core_security_list" "wrkrs_security_list":
>   60:   egress_security_rules = {
> 
> The argument "egress_security_rules" was already set at main.tf:54,3-24. Each
> argument may be set only once.
> 
> 
> Error: Attribute redefined
> 
>   on main.tf line 66, in resource "oci_core_security_list" "wrkrs_security_list":
>   66:   egress_security_rules = {
> 
> The argument "egress_security_rules" was already set at main.tf:54,3-24. Each
> argument may be set only once.
> 
> 
> Error: Attribute redefined
> 
>   on main.tf line 72, in resource "oci_core_security_list" "wrkrs_security_list":
>   72:   egress_security_rules = {
> 
> The argument "egress_security_rules" was already set at main.tf:54,3-24. Each
> argument may be set only once.
> 
> 
> Error: Attribute redefined
> 
>   on main.tf line 84, in resource "oci_core_security_list" "wrkrs_security_list":
>   84:   ingress_security_rules = {
> 
> The argument "ingress_security_rules" was already set at main.tf:78,3-25. Each
> argument may be set only once.
> 
> 
> Error: Attribute redefined
> 
>   on main.tf line 90, in resource "oci_core_security_list" "wrkrs_security_list":
>   90:   ingress_security_rules = {
> 
> The argument "ingress_security_rules" was already set at main.tf:78,3-25. Each
> argument may be set only once.
> 
> 
> Error: Attribute redefined
> 
>   on main.tf line 96, in resource "oci_core_security_list" "wrkrs_security_list":
>   96:   ingress_security_rules = {
> 
> The argument "ingress_security_rules" was already set at main.tf:78,3-25. Each
> argument may be set only once.
> 
> 
> Error: Missing key/value separator
> 
>   on main.tf line 101, in resource "oci_core_security_list" "wrkrs_security_list":
> 
> Error: Invalid argument name
> 
>   on main.tf line 101, in resource "oci_core_security_list" "wrkrs_security_list":
>  101:       "min" = 22
> 
> Argument names must not be quoted.
> 
> 
> Error: Invalid argument name
> 
>   on main.tf line 102, in resource "oci_core_security_list" "wrkrs_security_list":
>  102:       "min" = 22
> 
> Argument names must not be quoted.
> 
> 
> Error: Unsupported argument
> 
>   on main.tf line 113, in resource "oci_core_security_list" "lb_security_list":
>  113:   egress_security_rules = [{
> 
> An argument named "egress_security_rules" is not expected here. Did you mean
> to define a block of type "egress_security_rules"?
> 
> Error: Unsupported argument
> 
>   on main.tf line 119, in resource "oci_core_security_list" "lb_security_list":
>  119:   ingress_security_rules = [{
> 
> An argument named "ingress_security_rules" is not expected here. Did you mean
> to define a block of type "ingress_security_rules"?


